import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'//import boostrap
import AppNavbar from '../components/NavBar';
import { UserProvider } from '../contexts/UserContext' 
import AppHelper from '../app-helper'
import { Container } from 'react-bootstrap'; 

function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({ email: null })

	useEffect(()=> {
		if (AppHelper.getAccessToken() !== null) {
			const options = {
				headers: { Authorization: `Bearer ${AppHelper.getAccessToken()}` }
			}
			fetch("https://floating-tundra-79426.herokuapp.com/api/users/details", options).then((response) => response.json()).then((userData) => {
				// 
				if (typeof userData.email != "undefined") {
					setUser({ email: userData.email })
				} else {
					// 
					setUser({ email: null })
				}
			})
		}
	}, [user.id]	

	)

	const unsetUser = () => {
		localStorage.clear()
		setUser({ email: null })
	}
  return(
  	<> 
  		<UserProvider value= { { user, setUser , unsetUser} }>
	  	<AppNavbar />
	    <Container>
	    
	  	  <Component {...pageProps} />
	  	</Container>
	  	</UserProvider>
  	</>
  	)
}

export default MyApp
