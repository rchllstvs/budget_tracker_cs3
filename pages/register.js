import {useState, useEffect } from 'react'
import {Form,Button, Row, Col} from 'react-bootstrap'
import Link from 'next/link'; 
import Router from 'next/router'

//import Swal
import Swal from 'sweetalert2'

export default function Register(){
    /*What do we bind to our input to track user input in real time?*/
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState(0)
	const [password1,setPassword1] = useState("")
	const [password2,setPassword2] = useState("")

	//stare for conditional rendering for the submit button
    const [isActive, setIsActive] = useState(true)

	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName,lastName,email, mobileNo, password1, password2]) 
    //lets now create our register method
    function registerUser(e) {
       e.preventDefault() //to avoid page redirection. 
       //lets check if an email exists in our records.
       fetch('https://floating-tundra-79426.herokuapp.com/api/users/email-exists', {
 	         method: 'POST',
 	         headers: {
 	         	'Content-Type': 'application/json'
 	         },
 	         body: JSON.stringify({
 	         	email: email
 	         }) //convert the value to string data type to be accepted in the API
       }).then(res => res.json()).then(data => {
       	  //lets create a checker just to see if may nakukuha tayu na data
       	  console.log(data)
       	  //lets create a control structure to give the appropriate response according to the value of the data. 
       	  if(data === false){
       	  	//if the return in false then allow the user to register otherwise NOPE
       	  	fetch('https://floating-tundra-79426.herokuapp.com/api/users/register', {
       	  		method: 'POST',
       	  		headers: {
       	  			'Content-Type': 'application/json'
       	  		},
       	  		body: JSON.stringify({
       	  			firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo,
					password: password1
       	  		})
       	  	}).then(res => res.json()).then(data => {
       	  		//you can create a checker here if you wish
       	  		console.log(data)
       	  		if(data === true) {
       	  			Swal.fire({
       	  				icon: "success",
       	  				title: "Successfully Registered",
       	  				text: "Thank you for registering."
       	  			})
       	  			//after displaying a success message redirect the user to the login page
       	  			Router.push('/')
       	  		}else{
       	  			Swal.fire({
       	  				icon: "error",
       	  				title: "Registration failed",
       	  				text: "Something went wrong"
       	  			})
       	  		}
       	  	})
       	  } else {
       	  	//the else branch will run if the return value is true
       	  	Swal.fire({
       	  		icon: 'error',
       	  		title: 'Registration Failed',
       	  		text: 'Email is already taken by someone else, Move on!'
       	  	})
       	  }
       })
    } 
   return (

		<>
		
		
		 <div className="login-box">
		
			<Form onSubmit={e => registerUser(e)} className="mb-3">
			<Row>
				<Col>
				<Form.Group controlId="userFirstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
				</Form.Group>
				</Col>
				<Col>
				<Form.Group controlId="userLastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
				</Form.Group>
				</Col>
			</Row>
				<Form.Group controlId="userEmail">
					<Form.Label>Email</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="number" placeholder="Enter Mobile No." value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
				</Form.Group>

				<Row>
				<Col>
				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
				</Form.Group>
				</Col>
				<Col>
				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
				</Form.Group>
				</Col>
				</Row>
				{
					isActive ?
				<Button variant="success" type="submit">Register</Button>
					:
				<Button variant="success" disabled>Register</Button>
			}
			</Form>
			</div>
		</>
		) 
}