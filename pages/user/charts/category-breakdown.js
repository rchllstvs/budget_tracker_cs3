import { useState, useEffect } from 'react'
import { InputGroup, Form, Col } from 'react-bootstrap'
import { Pie } from 'react-chartjs-2'
import View from '../../../components/View'
import randomcolor from 'randomcolor'
import moment from 'moment'
import AppHelper from '../../../app-helper';



export default () => {
   
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [bgColors,setBgColors] = useState([])
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))

    const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: bgColors
            }
        ]
    };

     useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }
         fetch(`https://floating-tundra-79426.herokuapp.com/api/users/get-record-breakdown-by-range`, payload)
          .then((res) => res.json())
          .then((records) => {
            setLabelsArr(records.map((record) => record.categoryName));
            setDataArr(records.map((record) => record.totalAmount));
            setBgColors(records.map(() => randomcolor()));
      });


    }, [fromDate, toDate])

    
    return (
        <View title="Category Breakdown">
            <h3>Category Breakdown</h3>
            <Form.Row>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>From</Form.Label>
                    <Form.Control type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                </Form.Group>
                <Form.Group as={ Col } xs="6">
                    <Form.Label>To</Form.Label>
                    <Form.Control type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                </Form.Group>
            </Form.Row>
            <hr/>
            <Pie data={data}/>
        </View>
    )
}
