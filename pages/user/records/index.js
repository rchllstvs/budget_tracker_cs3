import { useState, Fragment, useEffect } from 'react'
import { Card, Button, Row, Col, InputGroup, FormControl, Form } from 'react-bootstrap'
import moment from 'moment'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';


export default () => {
    const [searchKeyword, setSearchKeyword] = useState('')
    const [searchType, setSearchType] = useState("All")
  

    return (
        <View title="Records">
            <h3>Records</h3>
            <InputGroup className="mb-2">
                <InputGroup.Prepend>
                    <Link href="/user/records/new"><a className="btn btn-success">Add</a></Link>
                </InputGroup.Prepend>
                <FormControl placeholder="Search Record" value={ searchKeyword } onChange={ (e) => setSearchKeyword(e.target.value) }/>
                <Form.Control as="select" defaultValue={ searchType } onChange={ (e) => setSearchType(e.target.value) }>
                    <option value="All">All</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>

            </InputGroup>
            <RecordsView searchKeyword={ searchKeyword } searchType={ searchType }/>
        </View>
    )
}

const RecordsView = () => {
    const [records, setRecords] = useState([])
     useEffect(()=> {
    
         fetch(`https://floating-tundra-79426.herokuapp.com/api/users/get-record`,{
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`}
          })
          .then(res=> res.json()).then(data=> setRecords(data) )

           
    }, [])
       
       console.log(records)
    return (
        <Fragment>
            {
                records.map((record) => {
                    console.log(record)
                    const textColor = (record.type === 'Income') ? 'text-success' : 'text-danger'
                    const amountSymbol = (record.type === 'Income') ? '+' : '-'

                    return (
                        <Card className="mb-3" key={ record._id }>
                            <Card.Body className="card">
                                <Row>
                                    <Col xs={ 6 }>
                                        <h4>{ record.description }</h4>
                                        <h5><span className={ textColor }>{ record.type }</span> { ' (' + record.categoryName + ')' }</h5>
                                        <h6>{ moment(record.dateAdded).format("MMMM D, YYYY") }</h6>
                                    </Col>
                                    <Col xs={ 6 } className="text-right">
                                        <h6 className={ textColor }>{ amountSymbol + ' ' + record.amount.toLocaleString() }</h6>
                                        <span className={ textColor }>{ record.balanceAfterTransaction.toLocaleString() }</span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>

                    )
                })
            }
        </Fragment>
    )
}




