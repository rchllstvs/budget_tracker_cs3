import { useState, Fragment, useEffect } from 'react'
import { Table, Card, Row, Col, Button } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';

export default () => {
   
    const [categories, setCategories] = useState([])
     useEffect(()=> {
        
             fetch(`https://floating-tundra-79426.herokuapp.com/api/users/get-all-category`,{
                  method: 'GET',
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${AppHelper.getAccessToken()}`}
              })
              .then(res=> res.json()).then(data=> setCategories(data) )

               
        }, [])
        


    return (
        <View title="Categories">
            <h3>Categories</h3>
            <Link href="/user/categories/new"><a className="btn btn-success mt-1 mb-3">Add</a></Link>
            <Table striped bordered hover>
                <thead >
                    <tr >
                        <th >Category </th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
         <Fragment>
            {
                categories.map((category) => {
                       return (
                        <tr>
                        <td>
                        <Card className="mb-3" key={ category._id }>
                            <Card.Body>
                                <Row>
                                    <Col xs={ 6 }>
                                        <h5>{ category.name }</h5>
                                        
                                    </Col>
                                   
                                </Row>
                            </Card.Body>
                        </Card>
                        </td>

                        <td>
                        <Card className="mb-3" key={ category._id }>
                            <Card.Body>
                                <Row>
                                    <Col xs={ 6 }>
                                        <h5>{ category.typeName }</h5>
                                        
                                    </Col>
                                  
                                </Row>
                            </Card.Body>
                        </Card>
                        </td>
                        </tr>
                    )
                })
            }
        </Fragment>
                </tbody>
            </Table>
        </View>
    )
}
