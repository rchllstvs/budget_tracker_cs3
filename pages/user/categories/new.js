import { useState } from 'react'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'
import View from '../../../components/View'
import AppHelper from '../../../app-helper'

 const NewCategoryForm =() => {

    const [categoryName, setCategoryName] = useState('')
    const [typeName, setTypeName] = useState(undefined)

function createCategory(e){
    e.preventDefault()
    //describe the request to login
    fetch("https://floating-tundra-79426.herokuapp.com/api/users/add-category", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${AppHelper.getAccessToken()}`
      },
      body: JSON.stringify({
        name: categoryName,
        type: typeName
      }) //for the request to be accepted by the api
    }).then(res => res.json()).then(data => {
      //lets create a control structure
      console.log(data)
      if( data === true) {
        //store the access token in the local storage
       
        Swal.fire({
          icon: 'success',
          title: 'Successfully added'
        })
     
      } else {
           Swal.fire('Login Error', 'You may have registered using a different login procedure', 'error')
      }
    })
  }






    // return (
    //     <View title="New Category">
    //         <Row className="justify-content-center">
    //             <Col xs md="6">
    //                 <h3>New Category</h3>
    //                 <Card>
    //                     <Card.Header>Category Information</Card.Header>
    //                     <Card.Body>
    //                         <NewCategoryForm/>
    //                     </Card.Body>
    //                 </Card>
    //             </Col>
    //         </Row>
    //     </View>
    // )




    return (
      <View title="New Category">
       <Row className="justify-content-center">
         <Col xs md="6">
      <h3>New Category</h3>
       <Card.Header>Category Information</Card.Header>
        <Form onSubmit={ (e) => createCategory(e) }>
            <Form.Group controlId="categoryName">
                <Form.Label>Category Name:</Form.Label>
                <Form.Control type="text" placeholder="Enter category name" value={ categoryName } onChange={ (e) => setCategoryName(e.target.value) } required/>
            </Form.Group>
            <Form.Group controlId="typeName">
                <Form.Label>Category Type:</Form.Label>
                <Form.Control as="select" value={ typeName } onChange={ (e) => setTypeName(e.target.value) } required>
                    <option value selected disabled>Select Category</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </Form.Group>
            <Button variant="success" type="submit">Submit</Button>
        </Form>
        </Col>
         </Row>
         </View>
    )
}


export default NewCategoryForm;